$(document).ready(function(){
    $('.share').shared();
});

(function( $ ) {

    var methods = {

        init : function( options ) {

            var that = this;

            if(!that.length)
                return false;

            that
                .find('a')
                .on({

                    click: function(event){

                        if(!$(this).hasClass('nowindow')){

                            $(this).shared('shareWindow', options, event);

                        }

                    }

                });


        },
        shareWindow: function(options, event){

            var that = this;

            var defaults = {
                height: 430,
                width: 520,
                scrollbars: 'yes',
                status: 'yes'
            };

            var options = $.extend({}, defaults, options);

            var newWin = window.open(that.attr('href'), "JSSite", "menubar=yes,location=no,resizable=no,scrollbars=no,status=no");

                newWin.focus();

            event.preventDefault();

        }

    };

    $.fn.shared = function( method ) {

        if ( methods[method] ) {
            return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
        } else if ( typeof method === 'object' || ! method ) {
            return methods.init.apply( this, arguments );
        } else {
            $.error( 'Метода ' + method + ' не существует');
        }

    }

})(jQuery);