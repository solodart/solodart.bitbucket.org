// Вспомогательные фанкции
(function( $ ) {

    $.fn.lock = function() {

        this.addClass('lock');

    };

    $.fn.unlock = function() {

        this.removeClass('lock');

    };

    $.fn.toggleStr = function(event) {

        $(this).find('.str').toggleClass('hidden');
        event.preventDefault();

    };

    $.fn.xhrUpload = function() {

        var form = $(this)[0];

        var formData = new FormData(form);

        var url = $(form).prop('action');


        $.ajax({
            url: url,
            type: 'POST',
            xhr: function() {
                myXhr = $.ajaxSettings.xhr();
                if(myXhr.upload){
                    myXhr.upload.addEventListener('progress', progressHandlingFunction, false); // progressbar
                }

                return myXhr;

            },
            success: function(data) {

                if(data.beforeScript != '')
                    eval(data.beforeScript);


            },
            data: formData,
            dataType: 'json',
            cache: false,
            contentType: false,
            processData: false
        }, 'json')
        .fail(function(x) {

            console.log(x);
            console.log(x.responseText);

        });

    };

    $.fn.outerHTML = function(s) {
        return s
            ? this.before(s).remove()
            : jQuery("<p>").append(this.eq(0).clone()).html();
    };

})(jQuery);

function getParameterByName(link, name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(link);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

function parse_url(url){

    if(typeof url === 'undefined')
        return false;

    var href = document.createElement('a');
        href.href = url;

    return href;

}

function share_url(url, img, networks) {

    // дополним URL
    var url = (typeof url === 'undefined') ? window.location.href : url;
        url = parse_url(url);
        url = encodeURIComponent('http://' + url.hostname + url.pathname);

    var img = (typeof img === 'undefined') ? false : encodeURIComponent(img);

    var networks = (typeof networks === 'undefined') ? ['vk','facebook','twitter','googleplus','pinterest'] : (typeof networks === 'array') ? networks : networks.splice(',');

    var out = '';

    var shareUrls = {
        vk: function(url){
            return 'http://vk.com/share.php?url='+ url;
        },
        facebook: function(url){
            return 'https://www.facebook.com/sharer/sharer.php?app_id=&sdk=joey&u=' + url;
        },
        twitter: function(url){
            return 'https://twitter.com/intent/tweet?url=' + url + '?&counturl=' + url;
        },
        googleplus: function(url){
            return 'https://plus.google.com/share?url=' + url;
        },
        pinterest: function(url, img){

            var url = '?url=' + url;

            if(img)
                url += '&media=' + img;

            return 'http://www.pinterest.com/pin/create/button/'+url;
        }
    };

    for(var key in networks){

        if(shareUrls[networks[key]]){

            out += '<a href="'+shareUrls[networks[key]](url)+'" class="'+networks[key]+'"></a>';

        }

    }

    return '<div class="share">'+out+'</div>';

}

function str_repeat(input, multiplier) {
    var buf = '';
    for (; !!multiplier; multiplier >>= 1) {
        if (multiplier & 1) buf += input;
        input += input;
    }
    return buf;
}


function detDateString(date){

    var date = new Date(date);

    var month = ['января','февраля','марта','апреля','мая','июня','июля','августа','сентября','октября','ноября','декабря'];
        month =  month[date.getMonth()];

    var year = date.getFullYear();
    var day = date.getDate();

    return day+ ' ' + month + ' ' + year;


}

(function ($) {

    function isChecked(obj, val){

        return (obj.find('.val_'+val).size()>0);

    }


    var methods = {

        init: function (options) {

            var that = this;

            o = {
                multiple: true,
                cache: true,
                minChar: 3
            };


            if (options)
                $.extend(o, options);

            $('.autocomplite').on('click', '.listValues ul li a', function(e){
                $(this).Eautocomplete('listChecked');
                that.trigger('inputUpdateValue');
                e.preventDefault();
            }).on('click', '.selectedValues .delete', function(){
                if($(this).parents('.value').remove()){
                    that.trigger('inputUpdateValue');
                }
                e.preventDefault();
            });



            that
                .bind('blur', function(){

                    setTimeout(function(){
                        that.Eautocomplete('listHide');
                    },200);

                })
                .bind('focus keyup', function(){

                    if(that.val().length >= o.minChar){

                        that.Eautocomplete('check');

                    }
                    else if(that.val().length == 0){

                        that.Eautocomplete('listHide');

                    }
                });

        },
        listEmpty: function(){

            this.parents('.autocomplite').find('.listValues ul').empty();
            return this;

        },
        listHide: function(){

            this.parents('.autocomplite').find('.listValues').removeClass('open');
            return this;

        },
        listOpen: function(){

            this.parents('.autocomplite').find('.listValues').addClass('open');
            return this;

        },
        listChecked: function(){

            this.addClass('checked');
            var that = this.parents('.autocomplite');
            var val = this.data('value');

            if(that.hasClass('multiple')){

                if(!that.find('.val_'+val).size())
                    that.find('.selectedValues').prepend('<div class="value val_'+val+'">'+this.html()+'<input type="hidden" name="'+that.find('input').eq(0).data('name')+'" value="'+val+'"/><a href="##" class="operation delete"><span class="icon"></span></a></div>');

                that.trigger('inputUpdateValue');

            }else{

                if(!that.find('.val_'+val).size()){
                    that.find('.selectedValues').empty();
                    that.find('.selectedValues').prepend('<div class="value val_'+val+'">'+this.html()+'<input type="hidden" name="'+that.find('input').eq(0).data('name')+'" value="'+val+'"/><a href="##" class="remove"><span class="icon"></span></a></div>');

                }
                that.trigger('inputUpdateValue');
            }

        },
        listUpdate: function(values){

            var that = this;
            var parent = that.parents('.autocomplite');
            var list = parent.find('.listValues ul');

            that.Eautocomplete('listEmpty');

            for(var i in values){

                if(!isChecked(parent,i))
                    list.append('<li data-value="' + i + '">'+values[i]+'</li>');

                that.trigger('inputUpdateValue');

            }


        },
        check: function(){

            var that = $(this);
            var formData = 'data='+that.val();

            $.ajax({

                url: that.data('url'),
                type: 'post',
                data: formData,
                cache: o.cache,
                dataType: 'json',
                success: function(e){

                    if(Object.keys(e).length > 0){

                        that.Eautocomplete('listOpen').Eautocomplete('listUpdate', e);

                    }else{

                        that.Eautocomplete('listEmpty').Eautocomplete('listHide');

                    }

                },
                statusCode: {
                    500: function(d){
                        console.log(d.responseText);
                    }
                }

            });

        }
    };

    $.fn.Eautocomplete = function (method) {
        if (methods[method]) {
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || !method) {
            return methods.init.apply(this, arguments);
        } else {
            $.error('Метод ' + method + ' в jQuery.liTranslit не существует');
        }
    };
})(jQuery);


/**
 * Tabs
 */

(function( $ ) {

    var methods = {

        init : function( options ) {

            var that = this;

            $(document)
                .on('click', '.tab', function(){

                    var add = $(this).hasClass('add');

                    if(add)
                        that.tabs('add', $(this));
                    else
                        that.tabs('open', $(this));

                }).on('blur', '.tab a', function(){

                    that.tabs('updateTabInfo', $(this).parents('.tab'));

                }).on('click', '.tabPageMenu a', function( event ){

                    $(this).tabs('openTabPageMenu', event);
//                    event.preventDefault();

                });

        },
        openTabPageMenu : function( event ){

            var that = this;
            var tabPage = that.parents('.tabPage');
            var open = that.parent('li').data('open');

            var subMenu = (that.parent().hasClass('submenu'));

            if(subMenu)
            {
                that.parents('.tabPageMenu').find('ul').removeClass('open');
                that.parent('li').toggleClass('open');
            }
            else
            {
                that.parents('.tabPageMenu').find('li').removeClass('active');
                tabPage.find('.tabPage').removeClass('active');
                that.parent('li').addClass('active');
                tabPage.find(open).addClass('active');
            }

        },
//        updateTabInfo : function( tab ){
//
//            $('#' + tab.data('name')).val(tab.text());
//
//        },
        open : function( options ){

            var that = this;

            var index = options.index();

            that
                .find('.tab, .contentInner')
                .removeClass('active');
            that
                .find('.tab:eq(' + index + '), .contentInner:eq(' + index + ')')
                .addClass('active');


        }
//
//        add : function( options ){
//
//            var that = this;
//
//            var size = that.find('.tabs .tab').size();
//            var tab = that.find('.tabs .tab').eq(0).clone();
//                tab.find('a').text('Новая вкладка '+ that.find('.tabs .tab').size());
//                tab.data('name', 'tabs_name_'+(size));
//
//            var tabPage = '<div class="tabPage"><textarea name="tabs[value]['+size+']" class="redactor"></textarea></div>';
//
//            that.find('.tab.add').before(tab);
//            that.find('.tabPage').last().after(tabPage);
//            that.find('.tabPage .redactor').last().redactor({'lang':'ru','toolbar':true,'iframe':false,'css':'wym.css','minHeight':'600','cleanup':true,'fileUpload':'/post/fileUpload/attr/content','fileUploadErrorCallback':function(obj,json) { alert(json.error); },'imageUpload':'/post/imageUpload/attr/content','imageGetJson':'/post/imageList/attr/content','imageUploadErrorCallback':function(obj,json) { alert(json.error); }});
//            that.append('<input type="hidden" id="tabs_name_'+size+'" name="tabs[name]['+size+']" value="Новая вкладка '+ size+'"/>');
//
//            that.tabs('open', $(tab));
//
//        }

    }

    $.fn.tabs = function( method ) {

        if ( methods[method] ) {
            return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
        } else if ( typeof method === 'object' || ! method ) {
            return methods.init.apply( this, arguments );
        } else {
            $.error( 'Метода ' + method + ' не существует');
        }
    }

})(jQuery);


/**
 * Editable
 */

(function( $ ) {

    var methods = {

        init : function( options ) {

            var that = this;

            that.each(function(){

//                $(this).text($($(this).data('edit')).val());

            });

            $(document)
                .on('keyup', '.editable', function(){
                    $($(this).data('edit'))
                        .val($(this).text());
                });

        },

        updateValue : function( tab ){



        }

    }

    $.fn.EditableObject = function( method ) {

        if ( methods[method] ) {
            return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
        } else if ( typeof method === 'object' || ! method ) {
            return methods.init.apply( this, arguments );
        } else {
            $.error( 'Метода ' + method + ' не существует');
        }
    }

})(jQuery);

/**
 * Editable
 */

(function( $ ) {

    var methods = {

        init : function( options ) {

            var that = this;


            that.each(function(){

                var _that = $(this);

                if((_that.hasClass('labelOnValue'))){

                    _that.dropDownMenu('setLabel');

                }

            });

        },
        setLabel : function(){

            var that = this;

            var label = that.find('.inpt.selected, .inpt.checked').eq(0).text();

            that.find('.label').text(label);

            that.find('.inpt').on('inputUpdateValue', function(){

                that.find('.label').text($(this).text());

            });
        }

    };

    $.fn.dropDownMenu = function( method ) {

        if ( methods[method] ) {
            return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
        } else if ( typeof method === 'object' || ! method ) {
            return methods.init.apply( this, arguments );
        } else {
            $.error( 'Метода ' + method + ' не существует');
        }
    }

})(jQuery);

/**
 * Process
 */

(function( $ ) {

    var methods = {

        init : function( options ) {

            var that = this;
            options = {
                step : 20
            };

            var step = (typeof options.step == 'undefined') ? 10 : parseInt(options.step);

            var input = that.find('input');

            var initDraggable = {
                max:100,
                min:0,
                value: that.find('input').val(),

                slide: function(event, ui) {

                    input.val(ui.value);

                    that.trigger('sliderValueSetValue');

                }
            };

            that
                .find('.sliderValueArr')
                .click(function(event){

                    var that = $(this);

                    var plus = that.hasClass('up');

                    var val = parseInt(input.val());

                        val = (Math.ceil(val/10)*10);

                        val += (plus) ? step : -1 * (step);

                        if(plus && val > initDraggable.max)

                            val = initDraggable.max;

                        else if(!plus && val <= initDraggable.min)

                            val = initDraggable.min;

                        input.val(val);

                        that.parents('.sliderValue').find('.ui-slider-handle').css({'left': val+'%'});

                        that.trigger('sliderValueSetValue');

                        event.preventDefault();

                });

            that
                .find('.sliderRange')
                .slider(initDraggable);

        }

    };

    $.fn.sliderRange = function( method ) {

        if ( methods[method] ) {
            return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
        } else if ( typeof method === 'object' || ! method ) {
            return methods.init.apply( this, arguments );
        } else {
            $.error( 'Метода ' + method + ' не существует');
        }
    }

})(jQuery);

/**
 * Process
 */

(function( $ ) {

    var methods = {

        init : function( options ) {

            var that = this;

        },
        openProcess : function( message ){
            var that = this;

            message = (typeof message == 'undefined') ? 'Пожалуйста подождите' : message;

            that.process('closeProcess');

            $('body').lock();

            $('.blurInner').addClass('blur');

            $('body').append('<div id="process"><div class="pattern"></div><div class="message">'+message+'</div></div>')


        },
        closeProcess : function(){

            $('#process').remove();
            $('.blurInner').removeClass('blur');
            $('body').unlock();

        }

    };

    $.fn.process = function( method ) {

        if ( methods[method] ) {
            return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
        } else if ( typeof method === 'object' || ! method ) {
            return methods.init.apply( this, arguments );
        } else {
            $.error( 'Метода ' + method + ' не существует');
        }
    }

})(jQuery);

/**
 * watch
 * TODO добавить установку маски/шаблона времени
 */

(function( $ ) {

    var methods = {

        init : function( options ) {

            that = this;

            if(typeof that === 'undefined' || that.length == 0)
                return false;

            var interval = (typeof that.data('interval') !== 'undefined') ? that.data('interval') : 1;

            setInterval(function(){that.watch('run');}, interval * 1000, that);

        },
        run : function(params){

            var timezone = (typeof that.data('timezone') !== 'undefined') ? that.data('timezone') : 0;

            var date = new Date();

            var hours = date.getHours() + timezone;

            var minutes = (date.getMinutes()<10) ? '0'+date.getMinutes() : date.getMinutes();

            var seconds = date.getSeconds();

            that.html(hours + ":" + minutes);

        }

    };

    $.fn.watch = function( method ) {

        if ( methods[method] ) {
            return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
        } else if ( typeof method === 'object' || ! method ) {
            return methods.init.apply( this, arguments );
        } else {
            $.error( 'Метода ' + method + ' не существует');
        }
    }

})(jQuery);

/**
 * StranaMenu
 */

(function( $ ) {

    var methods = {

        init : function( options ) {

            var that = this;

            if(!that.length) return false;


            that
                .find('.menu_open')
                .on({
                    click : function(){
                        that.stranaMenu('openMenu');
                    }
                });

            that
                .find('.menu_close')
                .on({
                    click : function(){
                        that.stranaMenu('closeMenu');
                    }
                });

            that
                .find('.submenu .subsubmenu > a')
                .on({
                    click : function( event ){
                        //event.preventDefault();
                        that.stranaMenu('toggleSubmenu', $(this));
                    }
                });

            that
                .find('.tabs a')
                .on({
                    click : function( event ){
                        that.stranaMenu('openTab', $(this));
                    }
                });


        },
        openMenu : function(){

            var that = this;

            that
                .addClass('open_menu');


        },
        closeMenu : function(){

            var that = this;

            that
                .removeClass('open_menu');

        },
        toggleSubmenu : function(_that){

            var that = this;
            var parent = _that.parents('li');

            parent
                .toggleClass('open');

        },
        openTab : function(_that){

            var that = this;
            var open = _that.data('open');

            that
                .find('.tabs a')
                .removeClass('active');

            _that
                .toggleClass('active');

            that.find('.submenu').removeClass('active');
            that.find(open).addClass('active');



        }
    }

    $.fn.stranaMenu = function( method ) {

        if ( methods[method] ) {
            return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
        } else if ( typeof method === 'object' || ! method ) {
            return methods.init.apply( this, arguments );
        } else {
            $.error( 'Метода ' + method + ' не существует');
        }
    }

})(jQuery);

/**
 * Editable
 */

(function( $ ) {

    var methods = {

        init : function( options ) {

            var that = this;

            if(!that.length)
                return false;


            that
                .each(function(){

                    var title;
                    var location;
                    var _that = $(this);

                    if(_that.data('location') != '')
                        location = $(this).data('location');

                    if(_that.attr('href') != '' && location != '')
                        location = $(this).attr('href');

                    if(_that.data('title') != '' && !title)
                        title = $(this).data('title');

                    if(_that.attr('title') != '' && !title)
                        title = _that.attr('title');

                    if(_that.attr('alt') != '' && !title)
                        title = _that.attr('alt');

                    if(_that.text() != '' && !title)
                        title = _that.text();

                    _that.on({
                        click : function( event ){

                            that.HAPIopen('setLocation', location, title, event);

                        }
                    });

                });

        },
        setLocation : function( location, title, event ){

            var that = this;

            document.title = title;
            history.pushState(null, title, location);

            event.preventDefault();

        }

    }

    $.fn.HAPIopen = function( method ) {

        if ( methods[method] ) {
            return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
        } else if ( typeof method === 'object' || ! method ) {
            return methods.init.apply( this, arguments );
        } else {
            $.error( 'Метода ' + method + ' не существует');
        }
    }

})(jQuery);


/**
 * map
 */

(function( $ ) {

    var methods = {

        init : function( options ) {

            var that = this;

            if(!that.length)
                return false;

            $('.menuMap input').focus();

            $(document).bind('load ready', function(){
                that.gmap('resize');
            });

            $(window).bind('load resize', function(){
                that.gmap('resize');
            });

        },
        resize : function(){

            var that = this;

            var footerHeight = $('footer.footer').height();
            var windowHeight = $(window).height();

            that.css({height: (windowHeight-footerHeight)});

        },
        addMarkersWitchLabels : function(markers, map, init){

//            var that = this;

            if(typeof Markers != 'undefined'){
                for(var i in Markers)
                    Markers[i].setMap(null);
            }

            Markers = [];


            if(!init){
                $('.menuMap .link .pop').addClass('hidden');
                $('.menuMap .link .res').removeClass('hidden');
            }
            else{
                $('.menuMap .link .res').addClass('hidden');
                $('.menuMap .link .pop').removeClass('hidden');
            }

            if(!markers.length)
                $('.menuMap .link .pop').removeClass('hidden');

            if(markers.length == 1){
                map.setCenter(new google.maps.LatLng( markers[0].lat, markers[0].lng ))
            }

            for( var i in markers ){

                var position = new google.maps.LatLng( markers[i].lat, markers[i].lng );

                var label = new MarkerWithLabel({
                    position: position,
                    map: map,
                    title: markers[i].title,
                    labelContent: (typeof markers[i].href == 'string') ? '<span class="flagStaff" onclick="window.location.href = \''+markers[i].href+'\'"><span class="'+markers[i].labelClass+'"></span></span>' : '<span class="flagStaff"><span class="'+markers[i].labelClass+'"></span></span>',
                    icon: new google.maps.MarkerImage("/", new google.maps.Size(32,22), new google.maps.Point(0,0), new google.maps.Point(1,0))
                });

                Markers.push(label);
                //
                //var label = new MarkerWithLabel({
                //    position: position,
                //    map: map,
                //    title: markers[i].title,
                //    labelContent: (typeof markers[i].href == 'string') ? '<span class="'+markers[i].labelClass+'" onclick="window.location.href = \''+markers[i].href+'\'"></span>' : '<span class="'+markers[i].labelClass+'"></span>',
                //    icon: new google.maps.MarkerImage("/", new google.maps.Size(32,22), new google.maps.Point(0,0), new google.maps.Point(1,22))
                //});
                //
                //Markers.push(label);


                //var marker = new google.maps.Marker({
                //    position: position,
                //    map: map,
                //    title: markers[i].title,
                //    icon: new google.maps.MarkerImage("/images/gmap_marker.png", new google.maps.Size(32,22), new google.maps.Point(0,0), new google.maps.Point(1,0))
                //});
                //
                //if(typeof markers[i].href == 'string')
                //    google.maps.event.addListener(marker, "click", function () { window.location.href = markers[i].href });
                //
                //Markers.push(marker);

            }

            $('.menuMap .link .res').data('install', false).scrollAlterContent();

            return map;

        },
        find : function(markers, map){

            var that = this;

            $('body').gmap('addMarkersWitchLabels', markers, map, true);

            that.on({
                keyup: function(e){

                    var _that = $(this);

                    var init = false;
                    var val = _that.val();
                    var _val = _that.data('val');


                    if(
                        (e.keyCode != 8 && (e.keyCode <= 60 || e.keyCode >= 230)) ||
                        val == _val
                    )
                    {

                        return false;
                    }

                    if(val == ''){

                        $('.menuMap .link .res').addClass('hidden');
                        $('.menuMap .link .pop').removeClass('hidden');

                        $('body').gmap('addMarkersWitchLabels', markers, map, true);

                        return false;

                    }

                    $.ajax(
                        {
                            url:'/autocomplite/countriesmap',
                            data: 'q=' + _that.val(),
                            dataType: 'json',
                            type: 'post',
                            success: function(q){
                                if(q.data!=''){
                                    $('.menuMap .link .res').html(q.data);

                                    if(!q.markers.length) init = true;
                                        $('body').gmap('addMarkersWitchLabels', q.markers, map, init);

                                }
                            }
                        }
                    ).error(function(q){

                        console.log(q);

                    });

                    _that.data('val', val);

                    //var obj = findInObject(val, markers);
                    //
                    //if(!val.length) init = true;
                    //    $('body').gmap('addMarkersWitchLabels', obj, map, init);

                }
            });

            function findInObject(str, obj){

                var out = [];

                for(var i in obj)
                    if(obj[i].title.toLowerCase().indexOf(str.toLowerCase())>=0)
                        out.push(obj[i]);

                return out;

            }

        }

    }

    $.fn.gmap = function( method ) {

        if ( methods[method] ) {
            return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
        } else if ( typeof method === 'object' || ! method ) {
            return methods.init.apply( this, arguments );
        } else {
            $.error( 'Метода ' + method + ' не существует');
        }
    }

})(jQuery);



(function ($) {
    var methods = {
        init: function (options) {
            var o = {
                eventType:'keyup blur copy paste cut start',
                elAlias: $(this),				//Элемент, в который будет записываться результат транслитерации или false
                reg:'',							//'" "="-","ж"="zzzz"' or false or ''
                translated: function (el, text, eventType) {},
                caseType: 'lower',				// lower(default), upper - регистр выходных данных
                status:true						//
            };
            if (options) {
                $.extend(o, options);
            }

            return this.each(function(){
                var elName = $(this),
                    elAlias = o.elAlias.css({wordWrap:'break-word'}),
                    nameVal;

                elName.data({
                    status:o.status
                })

                var inser_trans = function(result,e) {
                    if(o.caseType == 'upper'){
                        result = result.toUpperCase();
                    }
                    if(elName.data('status') && o.elAlias){
                        if (elAlias.prop("value") !== undefined) {
                            elAlias.val(result);
                        }else{
                            elAlias.html(result);
                        }
                    }
                    if(result != ''){
                        if (o.translated !== undefined) {
                            var type;
                            if(e == undefined){
                                type = 'no event';
                            }else{
                                type = e.type;
                            }
                            o.translated(elName, result, type);
                        }
                    }

                }

                var customReg = function(str){
                    customArr = o.reg.split(',');
                    for(var i=0;i<customArr.length; i++){
                        var customItem = customArr[i].split('=');
                        var regi = customItem[0].replace(/"/g,'');
                        var newstr = customItem[1].replace(/"/g,'');
                        var re = new RegExp(regi,"ig");
                        str = str.replace(re,newstr)
                    }
                    return str
                }

                var tr = function(el,e){
                    if (el.prop("value") !== undefined) {
                        nameVal = el.val();
                    }else{
                        nameVal = el.text();
                    }
                    if(o.reg && o.reg != ''){
                        nameVal = customReg(nameVal)
                    }
                    inser_trans(get_trans(nameVal),e);
                };
                elName.on(o.eventType,function (e) {
                    var el = $(this);
                    setTimeout(function(){
                        tr(el,e);
                    },50)
                });
                tr(elName);
                function get_trans() {
                    en_to_ru = {
                        'а': 'a',
                        'б': 'b',
                        'в': 'v',
                        'г': 'g',
                        'д': 'd',
                        'е': 'e',
                        'ё': 'jo',
                        'ж': 'zh',
                        'з': 'z',
                        'и': 'i',
                        'й': 'j',
                        'к': 'k',
                        'л': 'l',
                        'м': 'm',
                        'н': 'n',
                        'о': 'o',
                        'п': 'p',
                        'р': 'r',
                        'с': 's',
                        'т': 't',
                        'у': 'u',
                        'ф': 'f',
                        'х': 'h',
                        'ц': 'c',
                        'ч': 'ch',
                        'ш': 'sh',
                        'щ': 'sch',
                        'ъ': '',
                        'ы': 'y',
                        'ь': '',
                        'э': 'e',
                        'ю': 'ju',
                        'я': 'ja',
                        ' ': '_',
                        'і': 'i',
                        'ї': 'i'
                    };

                    nameVal = nameVal.toLowerCase();
                    nameVal = trim(nameVal);
                    nameVal = nameVal.split("");
                    var trans = new String();
                    for (i = 0; i < nameVal.length; i++) {
                        for (key in en_to_ru) {
                            val = en_to_ru[key];
                            if (key == nameVal[i]) {
                                trans += val;
                                break
                            } else if (key == "ї") {
                                trans += nameVal[i]
                            };
                        };
                    };
                    return trans;
                }

                function trim(string) {
                    //Удаляем пробел в начале строки и ненужные символы
                    string = string.replace(/(^\s+)|'|"|<|>|\!|\||@|#|$|%|^|\^|\$|\\|\/|&|\*|\(\)|\|\/|;|\+|№|,|\?|:|{|}|\[|\]/g, "");
                    return string;
                };
            })
        },disable: function () {
            $(this).data({
                status:false
            })
        },enable: function () {
            $(this).data({
                status:true
            })
        }
    };
    $.fn.liTranslit = function (method) {
        if (methods[method]) {
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || !method) {
            return methods.init.apply(this, arguments);
        } else {
            $.error('Метод ' + method + ' в jQuery.liTranslit не существует');
        }
    };
})(jQuery);



(function ($) {

    function getWindow(href){

        href = href.replace(/#/g, '');

        window.location.hash = href;

        $.ajax({
            url: href,
            dataType: 'script',
            cache: false,
            beforeSend: function(){
                if($('.modalWindowWidget').length)
                    $('.modalWindowWidget').modalWindow('lure');
            },
            success: function(q){
                $('.modalWindowWidget').modalWindow('open');
            },
            complete: function(){
                $('.modalWindowWidget').modalWindow('open');
            },
            statusCode: {
                500: function(d){
                    console.log(d.responseText);
                }
            }
        });

    }

    var methods = {
        init: function (options) {

            var that = this;

            that.modalWindow('getWindow',window.location.href)

            if(!that.length)
                return false;

            $('body')
                .on('click','.modalWindowClose', function(){
                    $('.modalWindowWidget').modalWindow('close');
                })
                .on('click','.modalWindow', function(){
                    $(this).modalWindow('getWindow');
                });

        },
        getWindow: function(link){

            var that = this;

            if(!that.length)
                return false;

            if(typeof link == 'undefined')
                var link = that.prop('href');



            link = parse_url(link);

            if(typeof link.hash != 'undefined' && link.hash != '')
                getWindow(link.hash);


        },

        open: function(){

            var that = this;

            if(!that.length)
                return false;

            var isMaxHeight = (that.hasClass('maxHeight'));

            var isMaxWidth = (that.hasClass('maxWidth'));

            var shirm = that.find('.inner');

            var top = (shirm.height() > $(window).height())? 0 : ($(window).height()/2)-(shirm.height()/2);

            if(isMaxHeight){

                that.css({height:$(window).height()});

                that.find('.inner').css({height:$(window).height()});

            }

            if(isMaxWidth){

                that.css({width:$(window).width()});

                that.find('.inner').css({width:$(window).width()});

            }

            if(!isMaxHeight)
                shirm.css({ top:  top});

            $('.blurInner').addClass('blur');

            $('body').lock();

            that
                .find('.modalWindowShadow')
                .animate({'opacity':0.5}, 0, function(){
                    $(this).removeClass('close');
                });

            $('.scrollAlterContent').scrollAlterContent();

        },

        lure: function(){

            var that = this;

            if(!that.length)
                return false;

            that.remove();
        },

        close: function(){

            var that = this;

            if(!that.length)
                return false;

            that
                .find('.modalWindowShadow')
                .animate({'opacity': 0}, 0, function(){

                    $('.blurInner').removeClass('blur');
                    $('body').unlock();

                    $(this).addClass('close');

                    that.remove();
                });


            $(document).trigger('removeErrors');
        }

    };

    $.fn.modalWindow = function (method) {
        if (methods[method]) {
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || !method) {
            return methods.init.apply(this, arguments);
        } else {
            $.error('Метод ' + method + ' в jQuery.liTranslit не существует');
        }
    };
})(jQuery);

(function ($) {

    var methods = {

        init: function (options) {

            var that = this;

            $(document)
                .on('click', '.inpt', function(){

                    if($(this).hasClass('checkbox')){
                        $(this).formalizate('checkbox');
                    }

                    if($(this).hasClass('rechecker')){
                        $(this).formalizate('rechecker');
                    }

                })
                .on('click','.inpt.radiobuttonbox li:not(.checked)', function(event){

                    $(this).formalizate('radiobuttonbox');

                    event.preventDefault();

                })
                .on('click','.inpt.clearForm', function(event){

                    $(this).formalizate('clearForm');

                    event.preventDefault();

                })
                .on('click','.radiobuttons .inpt:not(.selected)', function(event){

                    $(this).formalizate('radiobutton', event);

                });


            $('.inpt.number').formalizate('number');


        },
        clearForm: function(){

            var that = this;

            var form = that.parents('form');

            var inpts = form.find('.inpt');

            inpts.each(function(){

                var inpt = $(this);

                var isCheckbox = (inpt.hasClass('checkbox'));

                if(isCheckbox){

                    inpt.removeClass('checked');

                    inpt.find('input').prop('checked', false);

                }


            });



            return this;

        },
        checkbox: function(){

            var that = this;

            var submenu = that.parent().parent();

            var isSubmenu = submenu.hasClass('submenu');

            var isOpen = submenu.hasClass('open');

            var isChecked = that.find('input').prop('checked');

            if(!isSubmenu || (isChecked == isOpen)){

                that.toggleClass('checked');

                that.find('input[type=checkbox]').prop('checked', !isChecked);
                console.log(that.parents('form').serializeArray());

            }

            if(isSubmenu){

                if(isOpen){

                    submenu.find('.inpt').removeClass('checked');

                    submenu.find('input').prop('checked', !isChecked);

                }else{

                    submenu.find('.inpt').addClass('checked');

                    submenu.find('input').prop('checked', !isChecked);

                }

            }

            var after = ($(this).data('after'))?eval($(this).data('after')):false;

            that.trigger('inputUpdateValue');

        },
        number: function(){

            var that = this;

            that
                .find('.val')
                .blur(function(){
                    var val = intToStr(strToInt($(this).text()));
                    $(this).text(val);
                    $(this).parent().find('input').val(val);
                });


            function strToInt(val){
                var i = parseInt(val.replace(/\s|\D+/g,""));
                return (isNaN(i))?0:i;
            }

            function intToStr(val){
                return val.toString().replace(/(\d{1,3})(?=((\d{3})*([^\d]|$)))/g, " $1 ");
            }


            $('body')
                .on('click','.inpt.number a', function(){

                    var that = $(this);
                    var parent = that.parents('.inpt');
                    var step = parent.data('step');
                    var plus = parent.hasClass('plus');
                    var val = parseInt(parent.find('input').val().replace(/\s/g,""));

                    var i = (that.hasClass('l'));
                    i = (i) ? val-step : val+step;
                    i = i.toString().replace(/(\d{1,3})(?=((\d{3})*([^\d]|$)))/g, " $1 ");

                    if(plus && i < 0)
                        i = 0;

                    parent.find('.val').text(i);
                    parent.find('input').val(i);

                });

            that.trigger('inputUpdateValue');

        },
        radiobuttonbox: function(){

            var that = this;
            var _that = that.parents('.radiobuttonbox');

            // модифицируемый объект
            var obj = (that.data('object')) ? $(that.data('object')) : false;
            // удалить класс у объекта $obj
            var classremove = (that.data('classremove')) ? that.data('classremove') : false;
            // добавить класс объекту $obj
            var classadd = (that.data('classadd')) ? that.data('classadd') : false;
            // не переключать состояние кнопок
            var setCheck = (_that.hasClass('nocheck'));

            if(obj){

                if(classadd)
                    obj.addClass(classadd);

                if(classremove)
                    obj.removeClass(classremove);

            }

            if(!setCheck){
                _that.find('li').removeClass('checked');
                that.addClass('checked');
            }

            var after = ($(this).data('after'))?eval($(this).data('after')):false;

            that.trigger('inputUpdateValue');
        },
        radiobutton: function(event){

            event.preventDefault();

            var that = this;
            var _that = that.parents('.radiobuttons');

            _that.find('.radiobutton').removeClass('selected');
            that.addClass('selected');
            _that.find('input').attr('checked', false);
            that.find('input').attr('checked', true);

            var after = ($(this).data('after'))?eval($(this).data('after')):false;

            that.trigger('inputUpdateValue');
        },
        rechecker: function(){

            var that = this;
            var span = that.find('span');
            var active = that.find('span.checked');
            var next = active.index() + 1;

            if(next >= span.size())
                next = 0;

            that.find('input').attr('checked', false);
            that.find('input').eq(next).attr('checked', true);
            active.removeClass('checked');
            span.eq(next).addClass('checked');

            that.trigger('inputUpdateValue');

        }

    };

    $.fn.formalizate = function (method) {
        if (methods[method]) {
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || !method) {
            return methods.init.apply(this, arguments);
        } else {
            $.error('Метод ' + method + ' в jQuery.liTranslit не существует');
        }
    };
})(jQuery);



(function ($) {

    var methods = {

        init: function (options) {

            var that = this;

            $('body')
                .on('click', '.errorsTMPClose', function(event){

                    event.preventDefault();

                    $('.errorsTMPClose, .errorsTMP').remove();

                })
                .on('ajaxFormSubmit', 'form', function(){

                    var that = $(this);

                    $(document).trigger('removeErrors');

                    that.find('.error').removeClass('error');

                    $.ajax({
                        url: that.attr('action'),
                        type: that.attr('method'),
                        data: that.serialize(),
                        dataType: 'json',
                        success: function(r){

                            if(r.afterScript)
                                eval(r.afterScript);

                            if(r.errors) // TODO: устаревшая переменная
                                that.ajaxForm('errorsAdd', r);
                            else if(r.errorsTMP) // TODO: переименовать в моделях после переименования метода в errors
                                that.ajaxForm('errorsTMP', r);
                            else
                                eval(r.js);

                            if(r.beforeScript)

                                eval(r.beforeScript);

                        },
                        statusCode: {
                            500: function(d){

                                console.log(d.responseText);

                            }
                        }
                    }).error(function(q){

                        console.log(q);

                    });

                    return false;

                });

        },
        errorsTMP: function(r){ // TODO: переименовать в errorsAdd после удаления устаревшего метода

            var errors = r.errorsTMP; // TODO: переименовать в моделях после переименования метода в errors

            $('body .errorsTMPClose').remove();

            $('body').append('<a class="errorsTMPClose" href="#CloseErrors"></a>');


            for(var i in errors){

                console.log(r, i);

                var inpt = $('input#'+ r.model + '_' + i);

                for(var x in errors[i]){

                    var position = inpt.offset();

                    var height = inpt.height() + 7;

                    $('body').append('<div class="errorsTMP" style="top:'+(position.top+height)+'px;left:'+position.left+'px;"><div class="arr"></div><span>'+errors[i][x]+'</span></div>');
                }

            }

        },
        errorsAdd: function(r){ // TODO: устаревший метод, почистить модельки

            for( var i in r.errors ){

                $('#'+ r.model +'_'+ i).addClass('error');
            }

        }
    };

    $.fn.ajaxForm = function (method) {
        if (methods[method]) {
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || !method) {
            return methods.init.apply(this, arguments);
        } else {
            $.error('Метод ' + method + ' в jQuery.liTranslit не существует');
        }
    };
})(jQuery);



(function ($) {

    function updFS(that){

        var inner = that.find('.scrollAlterContentInnerFix');

        var top = inner.scrollTop();
        var fsInner = that.find('.fsInner').height();
        var fs = that.find('.fs').height();
        var height_content = inner.find('.scrollAlterContentInner').height();



        that.find('.fs').css({top: (((top/height_content))*fsInner)+'px'});

    }

    var methods = {

        init: function (options) {

            var that = this;

            if(!that.length)
                return false;

            $(document)
                .on('resize',function(){

                    $(this).scrollAlterContent('install');

                });

            that.each(function(){
                $(this).scrollAlterContent('install');
            });


        },
        update: function(){

            var that = $(this);

            var parent = that.parent();

            var height = parent.height();

            var scrollAlterContentInnerFix = that.find('.scrollAlterContentInnerFix');

                scrollAlterContentInnerFix.css({maxHeight: height, top: 0});


        },
        scrollTo: function(to){

            var that = this;

            var srollOn = that.find('.scrollAlterContentInnerFix');

            srollOn
                .animate({
                    scrollTop : to
                }, 0, function(){
                    updFS(that);
                });

        },
        scroll: function(){

            var that = this;

            that.find('.fs').draggable({
                axis: 'y',
                containment: 'parent',
                drag: function( event ) {

                    var y,scroll;

                    var parents = $(this).parents('.scrollAlterContent');

                    var contentHeight = parents.find('.scrollAlterContentInner').height();

                    var innerOffsetTop = parents.find('.fsInner').offset().top;

                    var innerHeight = parents.find('.fsInner').height();

                    var barHeight = parents.find('.fs').height();

                    var thatTop = $(this).offset().top;

                    if(thatTop >= innerOffsetTop){

                        y = thatTop - innerOffsetTop;

                        scroll = (y / (innerHeight - (barHeight/2))) * contentHeight;

                        that.scrollAlterContent('scrollTo', scroll);

                    }

                }

            });

            that
                .on('click','.arr',function(){

                    var y = $(this).parents('.scrollAlterContent').height();

                    var isTop = ($(this).hasClass('t'));

                    var scroll = that.find('.scrollAlterContentInnerFix').scrollTop();

                        scroll += (isTop) ? -1 * y : y;

                    that.scrollAlterContent('scrollTo', scroll);

                });

        },
        install: function(){

            var that = $(this).eq(0);

            if(that.data('install'))
                return true;

            that.wrapInner('<div class="scrollAlterContentInner"></div>');

            that.wrapInner('<div class="scrollAlterContentInnerFix"></div>');

            that.append('' +
                '<div class="scrollAlter">'+
                    '<a class="arr t" href="#/t"></a>'+
                    '<a class="arr b" href="#/b"></a>'+
                    '<div class="fsInner">'+
                        '<a class="fs" href="#/c"></a>'+
                    '</div>'+
                '</div>');

            that.find('.scrollAlterContentInnerFix').scrollAlterContent('update');

            that.scrollAlterContent('scroll');

            setTimeout(function(){

                var scrollAlterContentInnerFix = that.find('.scrollAlterContentInnerFix');

                var height = that.parent().height();

                var height_content = that.find('.scrollAlterContentInner').height();

                that.scrollAlterContent('update');

                if(height_content > height){

                    that
                        .find('.scrollAlter')
                        .addClass('open');

                    that
                        .find('.scrollAlter .fs')
                        .css({height: (height/height_content) * 100 + '%'});

                    scrollAlterContentInnerFix.mousewheel(function(e){

                        scrollAlterContentInnerFix.animate({scrollTop: '+='+(e.deltaFactor * (-1 * e.deltaY))+'px'}, 0, function(){updFS(that)});

                        return false;

                    });

                }

            }, 150);

            $(this).data('install', true);

        }


    };

    $.fn.scrollAlterContent = function (method) {
        if (methods[method]) {
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || !method) {
            return methods.init.apply(this, arguments);
        } else {
            $.error('Метод ' + method + ' в jQuery.scrollAlterContent не существует');
        }
    };

})(jQuery);

(function ($) {

    var methods = {
        init: function (options) {

            var that = this;

            if(!that.length)
                return false;

            $(document).on('click', '.sort', function(){

                $(this).sort('sort');

            });

        },
        sort: function(){

            // сортировки
            var methodsSort = {
                asc: function (a,b){

                    a = (typeof a.toUpperCase !== 'undefined') ? a.toUpperCase() : a;
                    b = (typeof b.toUpperCase !== 'undefined') ? b.toUpperCase() : b;

                    return a > b ? 1 : ( a < b ? -1 : 0 );
                },
                desc: function (a,b){

                    a = (typeof a.toUpperCase !== 'undefined') ? a.toUpperCase() : a;
                    b = (typeof b.toUpperCase !== 'undefined') ? b.toUpperCase() : b;

                    return a > b ? -1 : ( a < b ? 1 : 0 );
                }
            };

            var that = this;

            // сортируемый список
            var list = $(that.data('sorted'));

            // сортируемые
            var items = list.find(that.data('sorteditem'));

            // направление сортировки
            var methodSort = 'asc';

            // сортируемое значение
            var value = 'datecreate';

            // в случае если комбинированный список сортировки
            if(that.find('input[name=sort]')){

                if(that.find('input[name=sortmethod]')){

                    methodSort = that.find('.inpt.radiobutton.selected input[name=sortmethod]').val();

                }

                if(that.find('input[name=sortvalue]')){

                    value = that.find('.inpt.radiobutton.selected input[name=sortvalue]').val();

                }


            }else{

                methodSort = that.data('method');

                methodSort = (methodSort == 'desc') ? 'asc' : 'desc';

                that.data('method', methodSort);

                value = that.data('value');

            }




            var form = items.parents('form');
            var out = [];
            var outVales = [];

            for(var i = 0; i < items.size(); i++){

                var val = items.eq(i).data(value);

                if(typeof outVales[val] == 'undefined') outVales[val] = [];

                out.push(val);
                outVales[val].push(items.eq(i));

            }

            //console.log(outVales);
            out = out.sort(methodsSort[methodSort]);

            //console.log(out);
            for(var i in out){

                list.append(outVales[out[i]]);

            }

            that.parents('form').trigger('sortForm');

        }



    };

    $.fn.sort = function (method) {
        if (methods[method]) {
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || !method) {
            return methods.init.apply(this, arguments);
        } else {
            $.error('Метод ' + method + ' в jQuery.sort не существует');
        }
    };

})(jQuery);


/**
 * imageFit
 * Подстраивает изображения под контейнер
 * TODO предварительно(frontend) писать ratio в data что бы можно было рассчитывать размер не дожидаясь загрузки документа
 */

(function( $ ) {

    var methods = {

        init : function( options ) {

            var that = this;

            that.each(function(){

                var img = $(this).find('img');

                if(img.data('ratio')) {

                    var ratio = parseFloat(img.data('ratio'));

                    that.imageFit('resize', ratio);

                }else{

                    var ratio = (Math.round((that.find('img').height()/that.find('img').width())*100)/100);

                }

                $(window).bind('load resize ready', function(){

                    that.imageFit('resize', ratio);

                });

            });



        },
        resize: function( ratio ){

            var that = this;

            var wHeight = that.height();
            var wWidth  = that.width();

            var ratioWindow = Math.round((wHeight/wWidth)*100)/100;

            var css = {};

            if(ratioWindow >= ratio)
                css = { marginTop: 0, marginLeft: (((wWidth * ratio) - wHeight) / 2), width: 'auto', height: wHeight };
            else
                css = { marginTop: -1 * (((wWidth * ratio) - wHeight) / 2), marginLeft: 0, width: wWidth, height: 'auto'  };

            that.find('img').css(css);
        }
    };

    $.fn.imageFit = function( method ) {

        if ( methods[method] ) {
            return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
        } else if ( typeof method === 'object' || ! method ) {
            return methods.init.apply( this, arguments );
        } else {
            $.error( 'Метода ' + method + ' не существует');
        }
    }

})(jQuery);

/**
 * movingPage
 * быстрый скролл страницы к шапке и обратно
 */

(function( $ ) {

    var methods = {

        init : function( options ) {

            var that = this;

            $(document).on('click', '.movingPage a', function(event){

                event.preventDefault();

                $(this).movingPage('move', that);

            });

        },
        move : function(that){

            var scrollTo = (typeof that.data('scrollTo') == 'undefined' || isNaN(parse_url(that.data('scrollTo')))) ? 0 : parse_url(that.data('scrollTo'));

            that.data('scrollTo', $(window).scroll());

            $('body').animate({
                scrollTop: scrollTo
            }, 1000);

        }
    };

    $.fn.movingPage = function( method ) {

        if ( methods[method] ) {
            return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
        } else if ( typeof method === 'object' || ! method ) {
            return methods.init.apply( this, arguments );
        } else {
            $.error( 'Метода ' + method + ' не существует');
        }
    }

})(jQuery);



/**
* fixedPosition
*
*/

(function( $ ) {

    var methods = {

        init : function( options ) {

            var that = this;
            var top = ($('.header').size()) ? $('.header').height() : 0;

            var pHei = that.height();                                 // высота объекта
                pHei += parseInt(that.css('paddingBottom'));
                pHei += parseInt(that.css('paddingTop'));

            options = {
                top: top,
                pHei: pHei
            };

            that.fixedPosition('changePosition', options)

            $(window).bind('scroll resize', function(){
                that.fixedPosition('changePosition', options)
            });

        },
        changePosition: function(options){

            if (!this.size()) return false;

            var that = this;

            var options = $.extend({parent: that.parent(), top: 0}, options);

            var wiTop = $(window).scrollTop();                        // окно проскроленно
            var wiHei = $(window).height();                           // высота окна
            var pTop = that.offset().top;                             // положение объекта

            var liTop = $(options.parent).offset().top;               // положение родительского элемента
            var liHei = $(options.parent).height();                   // высота родительского элемента
            var wiTop_l = parseInt($('body').data('scroll-last'));
            var wiScr_t = $('body').data('scroll-to');

            var css = {};                                           // стиль для фильтра по умолчанию
            var up = (wiTop < wiTop_l);

            $('body').data('scroll-last', wiTop);
            $('body').data('scroll-to', (up || wiScr_t == undefined) ? 'up' : 'down');

            if (liHei > options.pHei)

                if (wiHei >= options.pHei) { // объект меньше родительского элемента (помещается)

                    if (wiTop + options.top > liTop) // если положение родительского элемента ниже объекта
                    {
                        css = { position: 'fixed', top: options.top };

                        if ((options.pHei + wiTop) > ( (liTop + liHei) - options.top)) // если родительский элемент ниже объекта
                        {

                            css = { position: 'absolute', top: (liHei - options.pHei) };

                        }

                    }
                    else {

                        css = { position: 'relative', top: 0 };

                    }

                }
                else { // объект больше родительского элемента (не помещается)

                    var pHeiP = options.pHei - wiHei;

                    if (pTop >= liTop && pTop + options.pHei <= liTop + liHei) {

                        if (up && pTop >= wiTop && pTop > liTop) {

                            css = {position: 'fixed', top: options.top};

                        }
                        else if (!up && pTop + options.pHei <= wiTop + wiHei && pTop + options.pHei < liTop + liHei) {

                            css = {position: 'fixed', top: (-1 * pHeiP)};

                        }
                        else {

                            css = {position: 'absolute', top: (pTop - liTop)};

                        }
                    }
                    else {

                        if (pTop <= liTop) {

                            css = {position: 'relative', top: 0};

                        }

                        if (pTop + options.pHei >= liTop + liHei) {

                            css = {position: 'absolute', top: (liHei - options.pHei)};

                        }

                    }

                }

            // todo прибавить вместо 100 отступы(padding,margin)
            if((liHei) <= (that.height()+100)){

                css = {position: 'relative', top: 0};

            }

            that.css(css);

        }
    };

    $.fn.fixedPosition = function( method ) {

        if ( methods[method] ) {
            return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
        } else if ( typeof method === 'object' || ! method ) {
            return methods.init.apply( this, arguments );
        } else {
            $.error( 'Метода ' + method + ' не существует');
        }
    }

})(jQuery);


/**
* viewerPhoto
*/

(function( $ ) {

    function getItem(src,key,first){

        var _class = (first) ? 'active':'hidden';
            _class += (key===0) ? ' first':'';

        return '<div class="slide slide_'+key+' '+_class+'"><div class="imagesInner"><img class="img" src="'+src+'" /><img class="img_small" src="'+src+'"></div></div>';

    }

    function getSlideCast(items, active, info){

        var slider = '';

        var active = (typeof active === 'undefined') ? 0 : active;

        for( var x = 0; x < (items.length); x++){

            var _class = (active == x) ? 'active' : 'hidden';
            var slides = '';
            var points = '';

            if(items[x].length == 1){

                slides = getItem(items[x][0],0,(0===0));

            }else

                for( var y = 0; y < items[x].length; y++){

                    points += (y===0)?'<span class="point active"></span>':'<span class="point"></span>';

                    slides += getItem(items[x][y],y,(y===0));

                }


            slider += '<div class="viewerPhotoSlide '+_class+'"><div class="SlideCast" id="SlideCast_viewerPhoto_'+x+'"><div class="points">'+points+'</div>'+slides+''+info.eq(x).outerHTML()+'</div></div>';

        }

        return slider;
    }

    var methods = {

        init : function( options ) {

            var that = this;

            var object = (typeof that.data('object') === 'undefined') ? that.parent() : $(that.data('object'));

            var slider = [];

            var images = object.find('img[rel=photo]');

            var infoInner = object.find('.infoInner');

            for( var i = 0; i < images.length; i++){

                var img = $(images[i]);
                var slide = (typeof img.data('slide') !== 'undefined') ? parseInt(img.data('slide')) - 1 : slider.length + 1;

                if(!slider[slide])
                    slider[slide] = [];

                slider[slide].push(img.prop('src'));

            }

            $('body').append('<div class="viewerPhoto"><a class="close" href="#/close"><span class="activeNum">1</span> из '+slider.length+'</span><span class="closeSlider"></span></a><a class="next control link" href="#/next"><span class="arr"></span></a><a class="prev control link" href="#/prev"><span class="arr"></span></a><div class="inner sliderParallel"></div></div>');

            $('.viewerPhoto .inner').append(getSlideCast(slider,0,infoInner));

            $('.viewerPhoto .share').shared();

            $('.viewerPhoto').viewerPhoto('resize');

            $('.viewerPhoto .innerItems .parallelSlideItemsItem:not(.active) a').click(function(){

                $('.viewerPhoto').viewerPhoto('control', $(this).parent().index());

            });

            $(window).bind('load resize',function(){

                $('.viewerPhoto').viewerPhoto('resize');

            }).bind('keydown', function(event){

                if(event.keyCode == 39) $('.viewerPhoto').viewerPhoto('control', true);

                if(event.keyCode == 37) $('.viewerPhoto').viewerPhoto('control', false);

            });


            $('.viewerPhoto .likeHeart').each(function(){$(this).like()});

            $('.viewerPhoto .close').click(function(){$('.viewerPhoto').viewerPhoto('close');});

            $('.viewerPhoto .control').click(function(event){event.preventDefault();var next = $(this).hasClass('next'); if(event.shiftKey) next = !next; $('.viewerPhoto').viewerPhoto('control', next);});

        },
        control: function(next){

            var that = this;

            var slides = that.find('.viewerPhotoSlide');
            var active = that.find('.viewerPhotoSlide.active');
            var nextSlide = active.next().index();

            that.find('.viewerPhotoSlide').removeClass('active').addClass('hidden');

            if(typeof next === 'boolean'){

                if(next){

                    if((active.index() + 1) >= slides.size()){

                        nextSlide = 0;

                    }

                }else{

                    nextSlide = ((active.index()-1) >= 0) ? (active.index()-1) : (slides.size()-1);

                }

            }else if(typeof next === 'number'){

                nextSlide = next;

            }

            that.find('.activeNum').text(nextSlide+1);

            slides.eq(nextSlide).addClass('active').removeClass('hidden');

        },
        close: function(){

            $('.viewerPhoto').remove();

        },
        resize: function(){

            var that = this;
            var img = that.find('.active .active img');
            var inner = that.find('.inner');
            var marginX = parseInt(inner.css('marginLeft')) + parseInt(inner.css('marginRight'));
            var marginY = parseInt(inner.css('marginTop'))  + parseInt(inner.css('marginBottom'));

            var width  = $(window).width();
            var height = $(window).height();

                width  = (width < 1000) ? 1000 : width;
                height = (height < 800) ? 800  : height;

                width  = width - marginX;
                height = height - marginY;

            inner.css({width: width, height: height});

            var marginTop = (height > (img.height() - marginY)) ? (height - (img.height() - marginY))/2 : 0;

            that.find('.SlideCast').removeAttr('style').css({width: img.width(), marginTop: marginTop})

        }

    };

    $.fn.viewerPhoto = function( method ) {

        if ( methods[method] ) {
            return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
        } else if ( typeof method === 'object' || ! method ) {
            return methods.init.apply( this, arguments );
        } else {
            $.error( 'Метода ' + method + ' не существует');
        }
    }

})(jQuery);

/**
 * FileStorage
 */

(function( $ ) {

    var methods = {

        init : function() {

            var that = this;

            allChechedFileStorage = that.find('.checked').size();

            folderChechedFileStorage = that.find('._folder.checked').size();

            fileChechedFileStorage = that.find('._file.checked').size();

            //console.log(allChechedFileStorage, folderChechedFileStorage,fileChechedFileStorage);


            var menuBar = that.find('.menuBar');

            menuBar.fileStorage('updateClass');

            that.find('.action').on('click', function(event){$(this).fileStorage('action',that);event.preventDefault();});

            that.find('.item').on('click', function(){

                var checked = ($(this).hasClass('checked'));

                var file = ($(this).hasClass('_file'));

                var folder = ($(this).hasClass('_folder'));

                    (checked) ? allChechedFileStorage-- : allChechedFileStorage++;

                if(file)
                    (checked) ? fileChechedFileStorage-- : fileChechedFileStorage++;

                if(folder)
                    (checked) ? folderChechedFileStorage-- : folderChechedFileStorage++;
                //console.log(allChechedFileStorage, folderChechedFileStorage,fileChechedFileStorage);
                $(this).toggleClass('checked');

                $(this).find('a.inpt.checkbox.series').toggleClass('checked');

                $(this).find('a.inpt.checkbox.series input').prop('checked', !checked);

                menuBar.fileStorage('updateClass');

            });

        },
        action: function(menuBar){

            var that = this;

            var data = menuBar.find('form input[type=checkbox]').serializeArray();

            var open = (that.hasClass('open'));

            var view = (that.hasClass('view'));

            var edit = (that.hasClass('edit'));

            var move = (that.hasClass('move'));

            var clear = (that.hasClass('clear'));

            var remove = (that.hasClass('remove'));

            var replace = (that.hasClass('replace'));

            var restore = (that.hasClass('restore'));

            var download = (that.hasClass('download'));

            var createFolder = (that.hasClass('createFolder'));

            var paste = (that.hasClass('paste'));

            var select = (that.hasClass('select'));

            var pasteCancel = (that.hasClass('pasteCancel'));

            var request = {
                data: data,
                type: 'post',
                dataType: 'json',
                //dataType: 'html',
                success: function (q) {

                    console.log(q);

                    if(q.beforeScript)
                        eval(q.beforeScript);

                }
            };

            // Открыть папку
            if(open){

                var link = '#' + $('.item_' + (data[0].value)).data('link');

                window.location = link;

                $(document).modalWindow('getWindow', link);

            }

            // Редактировать название
            if(edit){

                $('.item_' + (data[0].value)).addClass('edit');

                var val = $('.item_' + (data[0].value) + ' .name input').val();

                $('.item_' + (data[0].value) + ' .name input')
                    .select()
                    .blur(function(){

                        $(this)
                            .parents('.item')
                            .removeClass('edit');

                        if(val != $(this).val()){

                            request.data = {name: $(this).val()};

                            request.url = '/filestorage/edit/' + data[0].value;

                            $.ajax(request);

                        }

                    });

            }

            // Заменить файл
            if(replace){

                var form = $('#fileStorage');

                var id = 'FileStorage_item_file_'+(data[0].value);

                $('.item_' + (data[0].value)).append('<input type="file" id="'+id+'" name="FileStorage[file]['+(data[0].value)+']"/>');


                var action = form.prop('action');

                form.prop('action', '/filestorage/replace/' + data[0].value);

                form
                    .find('#' + id)
                    .click()
                    .change(function(){
                        form.xhrUpload();
                    });


            }

            // Посмотреть
            if(view){

                var link = $('.item_' + (data[0].value)).data('link');

                window.open(link, '_blank');

            }

            // Выбрать для вставки
            if(select){

                request.url = '/filestorage/select';

                var insertTo = getParameterByName(window.location.hash, 'insertTo');

                var value = getParameterByName(window.location.hash, 'value');

                var mode = getParameterByName(window.location.hash, 'mode');

                var id = $('.itemDescription').size();

                switch (insertTo){

                    case '_new_':


                        request.url = '/filestorage/select?insertTo=new&id=' + id;

                        break;

                    default:

                        request.url = '/filestorage/select';

                        if(insertTo){

                            request.url += '?insertTo=' + insertTo;

                            request.url += '&id=' + insertTo;

                        }

                        request.url += '&mode=' + mode;

                        request.url += '&value=' + value;

                }


                $.ajax(request);

                return false;

            }

            // переместить выбраные файлы
            if(move){

                request.url = '/filestorage/move';

                $.ajax(request);

            }

            // переместить выбраные файлы
            if(paste){

                request.url = '/filestorage/paste';

                $.ajax(request);

            }

            // переместить выбраные файлы
            if(pasteCancel){

                request.url = '/filestorage/pasteCancel';

                $.ajax(request);

            }

            // Создать папку
            if(createFolder){

                request.url = '/filestorage/createFolder';

                $.ajax(request);

            }

            // Очистить корзину
            if(clear){

                request.url = '/filestorage/clear';

                $.ajax(request);

            }

            // Восстановить
            if(restore){

                request.url = '/filestorage/restore';

                $.ajax(request);

            }

            // Удалить
            if(remove){

                request.url = '/filestorage/remove';

                $.ajax(request);

            }

            // Скачать
            if(download){

                request.url = '/filestorage/download';

                $.ajax(request);

            }


        },
        updateClass: function(){

            var that = this;

            var items = that.find('.fll');

            items.removeClass('visible');

            var selector = '';

            if(fileChechedFileStorage >= 1) {

                selector += '._file';

            }

            if(folderChechedFileStorage >= 1) {

                selector += '._folder';

            }

            if(allChechedFileStorage == 1) {

                selector += '._one';

            }else if(allChechedFileStorage > 1){

                selector += '._multiple';

            }

            that.find(selector).addClass('visible');
        }

    };

    $.fn.fileStorage = function( method ) {

        if ( methods[method] ) {
            return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
        } else if ( typeof method === 'object' || ! method ) {
            return methods.init.apply( this, arguments );
        } else {
            $.error( 'Метода ' + method + ' не существует');
        }
    }

})(jQuery);

