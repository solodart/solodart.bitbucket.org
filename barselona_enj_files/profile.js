
$(document).ready(function(){

    $('.editImage a').click(function(event){
        $(this).parent().find('input').click();
        event.preventDefault();
    });

    $(window).bind('resize load', function(){

        var inner = $('.inner_cabinet .inner');

        //inner.css({width: inner.parent().width()});

    }).bind('scroll',function(event){

        var inner = $('.fixedInner .inner');

        var inner_parent = inner.parents('.inner_cabinet');

        var scroll_top = $(window).scrollTop() + 10 ;

        if(scroll_top >= $('.inner_cabinet .inner').parent().offset().top)

            inner_parent.addClass('fixed_inner');

        else

            inner_parent.removeClass('fixed_inner');
    });

    $('.editImage').parents('form').find('input[type=file]').change(function(event){

        var file = this.files[0];
        name = file.name;
        size = file.size;
        type = file.type;

        if(file.name.length < 1) {

        }
        else if(file.type != 'image/png' && file.type != 'image/jpg' && !file.type != 'image/gif' && file.type != 'image/jpeg' ) {
            alert("File doesnt match png, jpg or gif");
        }

        else {

            var form = $(this).parents('form')[0];

            var formData = new FormData(form);

            var url = $(form).prop('action');

            $.ajax({
                url: url,
                type: 'POST',
                xhr: function() {

                    myXhr = $.ajaxSettings.xhr();

                    if(myXhr.upload){

                        myXhr.upload.addEventListener('progress', progressHandlingFunction, false); // progressbar

                        $(document).process('openProcess', 'Пожалуйста подождите, фотографии обрабатываются');

                    }

                    return myXhr;

                },
                success: function(data) {

                    console.log(data);

                    $(document).process('closeProcess');

                    if(data.js != '')
                        eval(data.js);

                    if(data.update.length){
                        for(var i in data.update){
                            var x = data.update[i][1];
                            $(data.update[i][0]).attr('src', data.images[x]);
                        }
                    }

                },
                statusCode: {
                  500: function(d){

                      console.log(d.responseText);

                      $(document).process('closeProcess');

                  }
                },
                data: formData,
                dataType: 'json',
                cache: false,
                contentType: false,
                processData: false
            }, 'json')
            .fail(function(data) {

                console.log(form);
                console.log(form.responseText);

                $(document).process('closeProcess');

            });

            $(this).val(null);

        }
    });

    $('.sort').sort();

});


function progressHandlingFunction(x){

    if(!$('body').hasClass('progressbarOpen'))
        $('body').addClass('progressbarOpen');

    var progressbar = $('.progressbar');
    var progress = Math.floor((x.loaded / x.total) * 100);

    progressbar.find('.loading').css({width:progress+'%'});
    progressbar.find('.loading span').text(progress);
    if(progress == 100){
        setTimeout(function(){
            $('body').removeClass('progressbarOpen');
        }, 300);
    }
}


(function( $ ) {

    var methods = {

        init : function( options ) {



        }

    }

    $.fn.profile = function( method ) {

        if ( methods[method] ) {
            return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
        } else if ( typeof method === 'object' || ! method ) {
            return methods.init.apply( this, arguments );
        } else {
            $.error( 'Метода ' + method + ' не существует');
        }
    }

})(jQuery);