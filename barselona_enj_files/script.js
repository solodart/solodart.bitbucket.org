/**
 * Инициальзация
 */

$(document).ready(function(){

    $('.tabsInner').tabs();

    $('.nro').HAPIopen();

    $('.modalWindow').modalWindow();

    $('.editable').EditableObject();

    $('.strana_wrapper').stranaMenu();

    $('.gmap').gmap();

    $('.inpt').formalizate();

    $('.fixedPosition').fixedPosition();

    $('form').ajaxForm();

    $('.imageFit').imageFit();

    $('.sliderValue').sliderRange();

    $('.movingPage').movingPage();

    $('.dropDownMenu').dropDownMenu();


    $(document)
        .on('click', '.sort', function(){

            $(this).sort('sort');

        })
        .on('submit', '.ajaxForm', function(event){

            $(this).trigger('ajaxFormSubmit');

            return false;

        })
        .on('click', '.viewPhoto', function(event){

            $(this).viewerPhoto();

        })
        .on('click', '.toggleStr', function(event){

            $(this).toggleStr(event);

        })
        .on('click', '.toggleClass', function(){

            var that = $(this);

            var _that = that;

            var _class = (typeof that.data('class') != 'undefined') ? that.data('class') : 'open';

            if(typeof that.data('object') != 'undefined'){

                if(that.data('object') == '_parent_'){

                    _that = that.parent();

                }else{

                    _that = $(that.data('object'));

                }

            }

            _that.eq(0).toggleClass(_class);

            return false;

        })
        .on('removeErrors', document, function(){

            $('.errorsTMP').remove();

        })
        .on('click', '.ajaxLoad', function(event){

                var that = $(this);

                var url = that.prop('href');

                var data = that.data('data');

                    data = (typeof data !== 'undefined') ? $(data).serialize() : false;

                var insertTo = (typeof that.data('inserto') != 'undefined') ? $(that.data('inserto')) : false;

                var remove = (that.hasClass('remove'));

                var params = {
                    url: url,
                    type: 'post',
                    dataType: 'json',
                    data: data
                };

                if(remove && !confirm('Вы уверены что хотите удалить элемент?')){

                    return false;

                }

                $.ajax(params)
                    .done(function(data){

                        if(data.beforeScript)
                            eval(data.beforeScript);

                        if(insertTo)
                            insertTo.html(data.data);
                        else
                            $(data.data).insertBefore(that);

                        if(data.afterScript)
                            eval(data.afterScript);

                    }).fail(function(data){

                        console.log(data);

                    });


            event.preventDefault();

        });

    $('.scrollAlterContent').scrollAlterContent();

});

$(window)
    .bind('resize load ready', function(){

        var catalogPhotoAlbumsWidthMax = 383;

        var windowWidth = $(window).width();

        var article = $('#photosa .catalogItem.article').clone();

        $('#photosa .catalogItem.article').remove();

        var eq = 4;
        var st = 8;

        if(catalogPhotoAlbumsWidthMax * 2 > windowWidth){

            eq = 1;
            st = 4;

        }else if(catalogPhotoAlbumsWidthMax * 3 > windowWidth){

            eq = 1;
            st = 4;

        }else if(catalogPhotoAlbumsWidthMax * 4 > windowWidth){

            eq = 2;
            st = 7;

        }else if(catalogPhotoAlbumsWidthMax * 5 > windowWidth){

            eq = 3;
            st = 10;

        }else if(catalogPhotoAlbumsWidthMax * 6 > windowWidth){

            eq = 4;
            st = 8;

        }

        $('#photosa .catalogItem').removeClass('hidden').slice(st,-1).addClass('hidden');

        article.insertAfter($('#photosa .catalogItem').eq(eq));

    });
